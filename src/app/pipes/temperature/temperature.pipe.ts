import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'temperaturePipe'})
export class TemperaturePipe implements PipeTransform {
  transform(value: number, unit: string): number {
    let newValue: number = value;

    switch(unit) {
      case "Celsius":
        newValue = Math.round(newValue - 273.15);
        break;
      case "Fahrenheit":
          newValue = Math.round(((newValue - 273.15) * 9/5 + 32) * 100) / 100;
        break;
      default:
        break;
    }

    return newValue;
  }
}
