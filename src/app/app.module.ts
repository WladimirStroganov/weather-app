import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { WeatherSelectorComponent } from './weather-selector/weather-selector.component';
import { SelectComponent } from './select/select.component';
import { TemperaturePipe } from './pipes/temperature/temperature.pipe';

@NgModule({
  declarations: [
    AppComponent,
    WeatherSelectorComponent,
    SelectComponent,
    TemperaturePipe
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
