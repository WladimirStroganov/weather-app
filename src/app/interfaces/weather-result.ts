export interface IWeatherResult {
    weather: Array<IWeather>;
    main: IMain;
    wind: IWind;
}

interface IWeather {
    id: number;
    description: string;
}

interface IMain {
    temp: number;
}

interface IWind {
    speed: number;
}
