export default {
    apiUrl: "https://api.openweathermap.org/data/2.5/weather?q=",
    apiKey: "607a88f22aa82a950c04fb1f7c07b18e",
    cities: ["London", "Birmingham", "Manchester"],
    units: [
        "Kelvins",
        "Celsius",
        "Fahrenheit"
    ]
};
