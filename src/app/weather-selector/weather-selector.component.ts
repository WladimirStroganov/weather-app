import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from "@angular/forms";
import { HttpClient } from '@angular/common/http';
import config from "../config/config";
import { IWeatherResult } from "../interfaces/weather-result";

@Component({
  selector: 'app-weather-selector',
  templateUrl: './weather-selector.component.html',
  styleUrls: ['./weather-selector.component.css']
})
export class WeatherSelectorComponent implements OnInit {
  cities: Array<string> = config.cities;
  units: Array<string> = config.units;

  unitsControl = new FormControl();

  result: IWeatherResult;

  temperature: Number;
  unit: String;

  constructor(private http: HttpClient) {}

  ngOnInit() {}

  selectedChange(value: string) {
    if(!value) {
      this.result = null;
      return;
    }
    
    this.http.get<IWeatherResult>(config.apiUrl + value + '&APPID=' + config.apiKey).subscribe(result => {
      this.result = result;
      this.temperature = result.main.temp;
      this.unit = 'Kelvins'
    }, error => {
      console.error(error);
      this.result = null;
    });
  }

  unitsChange(unit: string) {
      this.unit = unit;
  }
}
