import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from "@angular/forms";
import { debounceTime } from "rxjs/operators";

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {
  @Input() options: Array<string>;
  @Input() results: Array<string>;
  @Output() selectedChange: EventEmitter<string> = new EventEmitter();

  searchControl = new FormControl();

  isOpen: boolean = false;

  selectedValue: string;

  constructor() { }

  ngOnInit() {
    this.searchControl.valueChanges
      .pipe(
        debounceTime(150)
      )
      .subscribe(value => this.search(value));
  }

  search(value: string) {
    value = value.toLowerCase();
    this.results = this.options.filter(option => option.toLowerCase().includes(value));
  }

  toggleResults(action: string) {
    action = action || this.isOpen ? 'hide' : 'show';

    if(action === 'hide') {
      this.isOpen = false;

      this.results = [];
    }
    else {
      this.isOpen = true;

      this.results = this.options;
    }
    
  }

  select(value: string) {
    this.selectedValue = value;

    this.toggleResults('hide');

    this.searchControl.setValue(value, {emitEvent: false});

    this.selectedChange.emit(this.selectedValue);
  }

  isEnter(key: string) {
    if(key === "Enter") {
      this.select(this.searchControl.value);
    }
  }
}
